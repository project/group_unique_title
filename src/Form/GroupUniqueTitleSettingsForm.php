<?php

namespace Drupal\group_unique_title\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines settings for the config form.
 */
class GroupUniqueTitleSettingsForm extends ConfigFormBase {
  use StringTranslationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new EntityDeleteLogSeetingsForm object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_unique_title_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'group_unique_title.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('group_unique_title.settings');

    $form['prefix'] = [
      '#markup' => $this->t('<b>NOTE:</b> This module is built
      around utilizing a 1:1 relationship of nodes to groups. If you have
      single nodes in multiple groups and/or group types, this module is
      not built for that at this time.'),
    ];

    $form['groupsEnabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require Unique Titles On Groups'),
      '#default_value' => ($config->get('groupsEnabled')) ? $config->get('groupsEnabled') : FALSE,
    ];

    $form['groupNodesEnabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require Unique Titles On Nodes'),
      '#default_value' => ($config->get('groupNodesEnabled')) ? $config->get('groupNodesEnabled') : FALSE,
    ];

    // Create a list of all group types.
    $form['groupTypes'] = [
      '#type' => 'details',
      '#title' => $this->t('Group Types'),
      '#description' => $this->t('Select what group types should be checked for unique titles.'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="groupsEnabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $group_type_options = [];
    $group_types = $this->entityTypeManager->getStorage('group_type')->loadMultiple();

    foreach ($group_types as $machine_name => $group_type) {
      $group_type_options[$machine_name] = $group_type->label();
    }

    $form['groupTypes']['group_bundles'] = [
      '#title' => $this->t('Group Types to Require Unique Title For'),
      '#type' => 'checkboxes',
      '#options' => $group_type_options,
      '#default_value' => ($config->get('group_bundles')) ? $config->get('group_bundles') : [],
    ];

    // Create checkboxes for all node types.
    $form['nodeTypes'] = [
      '#type' => 'details',
      '#title' => $this->t('Node Types'),
      '#description' => $this->t('Select what node types should be checked for unique titles.'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="groupNodesEnabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $node_type_options = [];
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    foreach ($node_types as $machine_name => $node_type) {
      $node_type_options[$machine_name] = $node_type->label();
    }

    $form['nodeTypes']['node_bundles'] = [
      '#title' => $this->t('Node Types to Require Unique Title For'),
      '#description' => $this->t('NOTE: Assumes a 1:1 relationship of node type to group. If you have single nodes in multiple groups and/or group types, this module is not built for that yet.'),
      '#type' => 'checkboxes',
      '#options' => $node_type_options,
      '#default_value' => ($config->get('node_bundles')) ? $config->get('node_bundles') : [],
    ];

    $form['excludedFormIds'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Excluded Form IDs'),
      '#description' => $this->t('Enter one form ID per line. The entered form IDs will not be processed for unique titles'),
      '#default_value' => ($config->get('excludedFormIds')) ? implode('\n', $config->get('excludedFormIds')) : [],
      '#states' => [
        'visible' => [
          [':input[name="groupsEnabled"]' => ['checked' => TRUE]],
          'or',
          [':input[name="groupNodesEnabled"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * PURPOSE: Stash form values in config settings for module.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('group_unique_title.settings')
      ->set('groupsEnabled', $form_state->getValue('groupsEnabled'))
      ->set('groupNodesEnabled', $form_state->getValue('groupNodesEnabled'))
      ->set('group_bundles', $this->cleanupMultipleCheckboxes($form_state->getValue('group_bundles')))
      ->set('node_bundles', $this->cleanupMultipleCheckboxes($form_state->getValue('node_bundles')))
      ->set('excludedFormIds', explode('\b', $form_state->getValue('excludedFormIds')))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Takes an array of checkboxes from a from element and removes 0 values.
   */
  protected function cleanupMultipleCheckboxes(array $checkbox_array): array {
    foreach ($checkbox_array as $key => $value) {
      if ($value === 0) {
        unset($checkbox_array[$key]);
      }
    }

    return $checkbox_array;
  }

}
