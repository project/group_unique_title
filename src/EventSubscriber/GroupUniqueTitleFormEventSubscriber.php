<?php

namespace Drupal\group_unique_title\EventSubscriber;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * EventSubscriber to capture group + group content verify unique title.
 */
class GroupUniqueTitleFormEventSubscriber implements EventSubscriberInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * Route matcher for current route.
   */
  private currentRouteMatch $routeMatch;

  /**
   * Config factory.
   */
  private ConfigFactory $configFactory;

  /**
   * Gets a list of all excluded forms from the module settings.
   *
   * @return array
   *   An array of excluded forms from the module settings.
   */
  public static function getExcludedFormIds(): array {
    return is_array(\Drupal::configFactory()->get('group_unique_title.settings')->get('excludedFormIds')) ?
      \Drupal::configFactory()->get('group_unique_title.settings')->get('excludedFormIds') : [];
  }

  /**
   * Gets a list of all node create forms from the module settings.
   *
   * @return array
   *   An array of node create forms relating to enabled node types in config.
   */
  public static function getNodeCreateFormIds(): array {
    $node_types = \Drupal::configFactory()->get('group_unique_title.settings')->get('node_bundles');
    $formids = [];

    foreach ($node_types as $node_type) {
      $formids[] = "node_" . $node_type . "_form";
    }

    return $formids;
  }

  /**
   * Gets a list of all node edit forms from the module settings.
   *
   * @return array
   *   An array of all node edit forms relating to enabled node types in config.
   */
  public static function getNodeEditFormIds(): array {
    $node_types = \Drupal::configFactory()->get('group_unique_title.settings')->get('node_bundles');
    $formids = [];

    foreach ($node_types as $node_type) {
      $formids[] = "node_" . $node_type . "_edit_form";
    }

    return $formids;
  }

  /**
   * Gets a list of allowed group types from config.
   *
   * @return array
   *   Array of allowed group types from module config.
   */
  public static function getEnabledGroupTypes(): array {
    return is_array(\Drupal::configFactory()->get('group_unique_title.settings')->get('group_bundles')) ?
      \Drupal::configFactory()->get('group_unique_title.settings')->get('group_bundles') : [];
  }

  /**
   * Is Group checking enabled?
   *
   * @return bool
   *   TRUE if enabled, FALSE if not!
   */
  public static function groupCheckingTitlesEnabled(): bool {
    return !is_null(\Drupal::configFactory()->get('group_unique_title.settings')->get('groupsEnabled')) ?
      \Drupal::configFactory()->get('group_unique_title.settings')->get('groupsEnabled') : FALSE;
  }

  /**
   * Is Node checking enabled?
   *
   * @return bool
   *   TRUE if enabled, FALSE if not!
   */
  public static function nodeCheckingTitlesEnabled(): bool {
    return !is_null(\Drupal::configFactory()->get('group_unique_title.settings')->get('groupNodesEnabled')) ?
      \Drupal::configFactory()->get('group_unique_title.settings')->get('groupNodesEnabled') : FALSE;
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $routeMatch
   *   Current route matcher.
   */
  public function __construct(ConfigFactory $config_factory, CurrentRouteMatch $routeMatch) {
    $this->configFactory = $config_factory;
    $this->routeMatch = $routeMatch;
  }

  /**
   * {@inheritdoc}
   *
   * Establish what events this subscriber responds to.
   *
   * To add specific base form forms:
   * 'hook_event_dispatcher.form_base_node_form.alter' => 'alterNodeForm',
   */
  public static function getSubscribedEvents(): array {
    return [
      FormHookEvents::FORM_ALTER => 'formAlter',
    ];
  }

  /**
   * EQUIVALENT: hook_form_alter().
   *
   * PURPOSE: Add validation of a unique title on adding a node to a group.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function formAlter(FormAlterEvent $event): void {
    $form_id = $event->getFormId();
    $form = &$event->getForm();
    $form_state = $event->getFormState();

    // Potentially add unique title validation to a group edit or add form.
    if (
      $this::groupCheckingTitlesEnabled()
      && isset($form) && isset($form_state)
      && (
        str_contains($form_id, '_group_add_form')
        || str_contains($form_id, '_group_edit_form')
      )
      && $form_id != 'field_group_add_form'
      && !in_array($form_id, $this::getExcludedFormIds())
    ) {
      // Check group type based on URL param when adding a new group.
      if (str_contains($form_id, '_group_add_form')) {
        $group_type_param = $this->routeMatch->getParameter('group_type');
        if (!is_null($group_type_param) && method_exists($group_type_param, 'id')) {
          $group_type = $group_type_param->id();
          if (in_array($group_type, $this::getEnabledGroupTypes())) {
            $form['#validate'][] = get_class($this) . '::groupValidateUniqueTitle';
          }
        }
      }

      // Check group type based on GROUP itself when editing.
      if (str_contains($form_id, '_group_edit_form')) {
        $group_type = $this->routeMatch->getParameter('group')->getGroupType()->id();
        if (in_array($group_type, $this::getEnabledGroupTypes())) {
          $form['#validate'][] = get_class($this) . '::groupValidateUniqueTitle';
        }
      }

    }

    if ($this::nodeCheckingTitlesEnabled()) {
      // If creating a node in a group, Potentially add validate unique title.
      if (
        in_array($form_id, $this::getNodeCreateFormIds())
        && !in_array($form_id, $this::getExcludedFormIds())
      ) {
        $group = $this->routeMatch->getParameter('group');
        if (!is_null($group)) {
          $form['#validate'][] = get_class($this) . '::validateUniqueTitle';
          return;
        }
      }

      // If editing a node in a group, Potentially add validate unique title.
      if (
        in_array($form_id, $this::getNodeEditFormIds())
        && !in_array($form_id, $this::getExcludedFormIds())
      ) {
        $node = $this->routeMatch->getParameter('node');
        $group = $this->getGroupByNodeId($node->id());
        if (!is_null($group)) {
          $form['#validate'][] = get_class($this) . '::validateUniqueTitle';
        }
      }
    }
  }

  /**
   * Validates that the title value is indeed unique in the current group.
   *
   * @todo Assumes a 1:1 ratio of nodes to groups & group types. Improve?
   *
   * @param array $form
   *   The form render array to alter.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form form state to work with if nescessary.
   */
  public static function validateUniqueTitle(array &$form, FormStateInterface $form_state): void {
    $form_id = $form['#form_id'];
    $node = $group = NULL;

    if ($form_state->hasValue('title')) {
      // NOTE: Can't DI RouteMatch due to static callback for form validate.
      $route_match = \Drupal::routeMatch();

      // Load the group based on which avenue the user came from.
      if (
        in_array($form_id, self::getNodeCreateFormIds(), TRUE)
      ) {
        /** @var \Drupal\group\Entity\Group $group */
        $group = $route_match->getParameter('group');
      }
      elseif (in_array($form_id, self::getNodeEditFormIds(), TRUE)) {
        $node = $route_match->getParameter('node');
        $group = self::getGroupByNodeId($node->id());
      }

      if ($group) {
        // Load all node group content.
        $all_group_content = [];
        $group_types = GroupType::loadMultiple();
        $installed_plugins = $group_types[$group->get('type')->getValue()[0]['target_id']]->getInstalledContentPlugins();
        foreach ($installed_plugins->getInstanceIds() as $plugin_id) {
          if (str_contains($plugin_id, 'group_node')) {
            $group_content = $group->getContentEntities($plugin_id);
            if ($group_content) {
              $all_group_content = array_merge($all_group_content, $group_content);
            }
          }
        }

        // Get the title, and load all group content, and see if title in use.
        $title = $form_state->getValue('title')[0]['value'];

        // Compare current node to all group content in the group.
        foreach ($all_group_content as $gc_node) {
          // Check we've got a node group content.
          if (
            $gc_node->getEntityTypeId() == 'node'
          ) {
            // Check if the titles match.
            if (
              trim($title) === trim($gc_node->getTitle())
            ) {
              // If $node, edit and check against all OTHER nodes by id compare.
              if (
                !isset($node)
                || (isset($node) && $node->id() != $gc_node->id())
              ) {
                $form_state->setErrorByName(
                  'title',
                  t(
                    'The title you have entered is currently in use by another node in the group. Click <a href="@node_url">here</a> to see the node.',
                    [
                      '@node_url' => $gc_node->toUrl()->toString(),
                    ]
                  )
                );
              }
              break;
            }
          }
        }
      }
    }
  }

  /**
   * Form validation for group add and group edit forms to check unique title.
   *
   * NOTE: Cannot DI as this is a static function for form validation callback.
   */
  public static function groupValidateUniqueTitle(array $form, FormStateInterface &$form_state): void {
    // Get current group ID and title.
    $gid = $form_state->getFormObject()->getEntity()->id();
    $title = $form_state->getValue('label')[0]['value'];

    // Get all groups matching the same title.
    $query = \Drupal::entityQuery('group');
    $query->condition('label', $title);
    $query->accessCheck(FALSE);
    $matching_title_gids = $query->execute();

    // Remove current group from list of results, if there are results.
    if ($gid && count($matching_title_gids) > 0) {
      foreach ($matching_title_gids as $key => $matching_gid) {
        if ($matching_gid === $gid) {
          unset($matching_title_gids[$key]);
        }
      }
    }

    // If there are results other than the current result, throw an error.
    if (count($matching_title_gids) > 0) {
      $form_state->setErrorByName('label',
        t('A group already exists with this title. Please use a different name.')
      );
    }
  }

  /**
   * Get the group that is related to a node.
   *
   * @param int $nid
   *   The node ID.
   *
   * @return \Drupal\group\Entity\GroupInterface|null
   *   The related group.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getGroupByNodeId(int $nid): ? GroupInterface {
    $group = NULL;
    // Load all the group content for this node.
    $gc = \Drupal::entityTypeManager()
      ->getStorage('group_content')
      ->loadByProperties([
        'entity_id' => $nid,
      ]);
    /** @var \Drupal\group\Entity\GroupContent $ent */
    $ent = reset($gc);
    if ($ent && $ent->getGroup()) {
      $group = $ent->getGroup();
    }
    return $group;
  }

}
